﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Harovsky.Models
{
    public class Car
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        [Required]
        public string Model { get; set; }

        [Required]
        public string Brand { get; set; }

        [Required]
        public DateTime ProdactionStartDate { get; set; }

        [Required]
        public string Color { get; set; }

        [Required]
        public string StoreId { get; set; }

        [Required]
        public string CarCategoryId { get; set; }



        public Store Store { get; set; }

        public CarCategory CarCategory { get; set; }
    }
}
