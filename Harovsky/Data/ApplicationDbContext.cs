﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harovsky.Models;

namespace Harovsky.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Models.Store> Stores { get; set; }

        public DbSet<Models.Car> Cars { get; set; }

        public DbSet<Harovsky.Models.CarCategory> CarCategory { get; set; }
    }
}
