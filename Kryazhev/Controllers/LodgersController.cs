﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Kryazhev.Data;
using Kryazhev.Models;

namespace Kryazhev.Controllers
{
    public class LodgersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LodgersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Lodgers
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Lodgers
                .Include(l => l.Home)?.ToList() ?? new List<Lodger>();

            applicationDbContext.ForEach(m => m.Home.Lodgers = new List<Lodger>());

            return View(applicationDbContext);
        }

        // GET: Lodgers/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lodger = await _context.Lodgers
                .Include(l => l.Home)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lodger == null)
            {
                return NotFound();
            }

            return View(lodger);
        }

        // GET: Lodgers/Create
        public IActionResult Create()
        {
            ViewData["HomeId"] = new SelectList(_context.Homes, "Id", "Id");
            return View();
        }

        // POST: Lodgers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FIO,Pnohe,Square,HomeId")] Lodger lodger)
        {
            if (ModelState.IsValid)
            {
                _context.Add(lodger);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["HomeId"] = new SelectList(_context.Homes, "Id", "Id", lodger.HomeId);
            return View(lodger);
        }

        // GET: Lodgers/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lodger = await _context.Lodgers.FindAsync(id);
            if (lodger == null)
            {
                return NotFound();
            }
            ViewData["HomeId"] = new SelectList(_context.Homes, "Id", "Id", lodger.HomeId);
            return View(lodger);
        }

        // POST: Lodgers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,FIO,Pnohe,Square,HomeId")] Lodger lodger)
        {
            if (id != lodger.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lodger);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LodgerExists(lodger.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["HomeId"] = new SelectList(_context.Homes, "Id", "Id", lodger.HomeId);
            return View(lodger);
        }

        // GET: Lodgers/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lodger = await _context.Lodgers
                .Include(l => l.Home)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lodger == null)
            {
                return NotFound();
            }

            return View(lodger);
        }

        // POST: Lodgers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var lodger = await _context.Lodgers.FindAsync(id);
            _context.Lodgers.Remove(lodger);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LodgerExists(string id)
        {
            return _context.Lodgers.Any(e => e.Id == id);
        }

        public IActionResult SquereFilter(int _squere) 
        {
            var result = _context.Lodgers
                .Where(m => m.Square > _squere)
                .Include(m => m.Home)
                ?.ToList() ?? new List<Lodger>();

            result.ForEach(m => m.Home.Lodgers = new List<Lodger>());

            return View("Index", result);
        }
    }
}
