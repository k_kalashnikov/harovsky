﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kryazhev.Migrations
{
    public partial class Init_database : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Homes",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    District = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    LoftNumber = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Homes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lodgers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    FIO = table.Column<string>(nullable: false),
                    Pnohe = table.Column<string>(nullable: false),
                    Square = table.Column<int>(nullable: false),
                    HomeId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lodgers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Lodgers_Homes_HomeId",
                        column: x => x.HomeId,
                        principalTable: "Homes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Lodgers_HomeId",
                table: "Lodgers",
                column: "HomeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Lodgers");

            migrationBuilder.DropTable(
                name: "Homes");
        }
    }
}
