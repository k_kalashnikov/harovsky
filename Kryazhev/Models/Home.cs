﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kryazhev.Models
{
    public class Home
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        [Required]
        public string District { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string LoftNumber { get; set; }

        
        public ICollection<Lodger> Lodgers { get; set; }
    }
}
