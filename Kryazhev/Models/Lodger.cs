﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kryazhev.Models
{
    public class Lodger
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        [Required]
        public string FIO { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Pnohe { get; set; }

        [Required]
        public int Square { get; set; }

        public string HomeId { get; set; }


        public Home Home { get; set; }
    }
}
